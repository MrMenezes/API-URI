from UriAPI import Uri
from functools import wraps
from flask import Flask, request, Response, url_for, redirect, render_template, abort, send_from_directory

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')
userUri = Uri()


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    global userUri
    userUri = Uri(username, password)
    return userUri.login_uri()


def authenticate():
    """Sends a 401 response that enables basic auth"""

    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})



def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


@app.route("/user")
@requires_auth
def user():
    global userUri
    return userUri.user_information()


@app.route('/problem/<number>')
def problem(number):
    if int(number) < 1 or int(number) > 8 or int(number) is None:
        return "PorFavor Insira um Problema entre 1 e 8"
    else:
        global userUri
        return userUri.get_problem(int(number))


@app.route('/problems')
def problems():
    global userUri
    return userUri.get_all_problem()


@app.route('/logout')
def logout():
    global userUri
    userUri = Uri()
    return authenticate()


@app.route('/')
def index():
    return '''
    <html>
      <head>
        <title>Uri API</title>
      </head>
      <body>
        <h1>URL Disponiveis</h1>
        <ul>
          <li> <a href="/user">Informacao do Usuario</a></li>
          <li> <a href="/problems">Todos Problemas</a></li>
          <li> <a href="/problem/0">Problema</a></li>
        </ul>
      </body>
    </html>'''

if __name__ == '__main__':
    app.debug = False
    app.run()
